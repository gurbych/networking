public class Main {
    public static void main(String[] args) throws Exception {

    	EchoServer server = new EchoServer();
    	new Thread(server).start();
		System.out.println(server.isRunning());      // false
		System.out.println(server.getPort());        // value of DEFAULT_PORT

		server.start();
		// server.isRunning()      // true
		// server.stop()
		// server.isRunning()      // false

		// server = new EchoServer(54321);
		// server.getPort()        // 54321
		// server.isRunning()      // false
		// server.start()
    }
}