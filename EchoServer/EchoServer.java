import java.net.ServerSocket;
import java.net.Socket;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class EchoServer implements Runnable {
	protected int port = 12345;
	protected boolean isRunning = false;
  protected ServerSocket serverSocket = null;
  protected Thread runningThread = null;
  protected ExecutorService threadPool = Executors.newFixedThreadPool(10);

	public EchoServer() {}

	public EchoServer(int port) throws EchoServerException {
		if ( port < 1024 || port > 65535 ) {
			throw new InvalidPortException();
		}
		this.port = port;
	}

  public synchronized boolean isRunning() {
   	return isRunning;
  }

  public int getPort() {
   	return this.port;
  }

  public void start() throws IOException {
    this.isRunning = true;
  }

  public void run(){
      synchronized(this){
          this.runningThread = Thread.currentThread();
      }
      openServerSocket();
      while(isRunning()){
          Socket clientSocket = null;
          try {
              clientSocket = this.serverSocket.accept();
          } catch (IOException e) {
              if(!isRunning()) {
                  System.out.println("Server Stopped") ;
                  return;
              }
              throw new RuntimeException("Error accepting client connection", e);
          }
          this.threadPool.execute(new WorkerRunnable(clientSocket));
      }
      this.threadPool.shutdown();
      System.out.println("Server Stopped") ;
  }

  private void openServerSocket() {
      try {
          this.serverSocket = new ServerSocket(this.port);
      } catch (IOException e) {
          throw new RuntimeException("Cannot open port", e);
      }
  }

  public synchronized void stop() {
  	this.isRunning = false;
    try {
      this.serverSocket.close();
    } catch (IOException e) {
      throw new RuntimeException("Error closing server", e);
    }
  }
}