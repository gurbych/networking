import java.io.BufferedReader;
import java.io.PrintWriter;
import java.io.InputStreamReader;
import java.io.IOException;
import java.net.Socket;

public class WorkerRunnable implements Runnable {

    protected Socket clientSocket = null;

    public WorkerRunnable(Socket clientSocket) {
        this.clientSocket = clientSocket;
    }

    public void run() {
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(),true);;
            String input, output;

            System.out.println("New socket was opened. Wait for messages...");
            while ((input = in.readLine()) != null) {
                if (input.equalsIgnoreCase("disconnect")) {
                    break;
                }
                out.println("Server echo: " + input);
                System.out.println(input);
            }

            out.close();
            in.close();
            System.out.println("Request processed. Socket was closed");
        } catch (IOException e) {
            //report exception somewhere.
            e.printStackTrace();
        }
    }
}