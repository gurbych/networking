import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) throws 
    	InvalidIpException, InvalidMaskException, 
    	ArgumentMissedException {

        // IPv4Address ip = new IPv4Address("127.12.45.22");
		// System.out.println(ip.toString());  // 127.12.45.22
		// System.out.println(ip.toLong());    // 2131504406		

		// IPv4Address ip = new IPv4Address(2131504406);
		// System.out.println(ip.toString());  // 127.12.45.22
		// System.out.println(ip.toLong());    // 2131504406		

		// System.out.println(ip.equals(new IPv4Address("127.12.45.22")));        // true
		// System.out.println(ip.equals(new IPv4Address(2131504406L)));           // true
		// System.out.println(ip.equals(new IPv4Address(0xF834AD02L)));           // false
		// System.out.println(ip.equals(new IPv4Address("189.11.23.211")));       // false
		// System.out.println(ip.greaterThan(new IPv4Address("131.16.34.66")));   // false
		// System.out.println(ip.lessThan(new IPv4Address("131.16.34.66")));      // true

		// IPv4Address address = new IPv4Address("192.168.0.0");
		// Network net = new Network(address, 24);

		// System.out.println(net.toString());                                // 192.168.0.0/24
		// System.out.println(net.getAddress().toString());                   // 192.168.0.0
		// System.out.println(net.getMask());
		// System.out.println(net.getFirstUsableAddress().toString());        // 192.168.0.1
		// System.out.println(net.getLastUsableAddress().toString());         // 192.168.0.254
		// System.out.println(net.getMaskString());                           // 255.255.255.0
		// System.out.println(net.getMaskLength());                           // 24
		// System.out.println(net.isPublic());                                // false
		// System.out.println(net.contains(new IPv4Address("10.0.23.4")));    // false
		// System.out.println(net.contains(new IPv4Address("192.168.0.25"))); // true
		// System.out.println(net.getBroadcastAddress().toString());          // 192.168.0.255
		// System.out.println(net.getTotalHosts());
		 // Network[] subnets = net.getSubnets();
		 // System.out.println(subnets[0].toString());                          // 192.168.0.0/25
		 // System.out.println(subnets[0].getAddress().toString());             // 192.168.0.0
		 // System.out.println(subnets[0].getFirstUsableAddress().toString()); // 192.168.0.1
		 // System.out.println(subnets[0].getLastUsableAddress().toString());  // 192.168.0.126
		 // System.out.println(subnets[0].getMaskLength());                    // 25

    	List<Route> routes = new ArrayList<Route>() {{
		   add(new Route(new Network(new IPv4Address("0.0.0.0"), 0), new IPv4Address("192.168.0.1"), "en0", 10));
		   add(new Route(new Network(new IPv4Address("192.168.0.0"), 24), null, "en0", 10));
		   add(new Route(new Network(new IPv4Address("10.0.0.0"), 8), new IPv4Address("10.123.0.1"), "en1", 10));
		   add(new Route(new Network(new IPv4Address("10.123.0.0"), 20), null, "en1", 10));
		}};

		Router router = new Router(routes);
		Route route = router.getRouteForAddress(new IPv4Address("192.168.0.176"));
		// // Route route = router.getRouteForAddress(new IPv4Address("192.168.0.1"));

		// System.out.println(route.getMetric());                  // 10
		// System.out.println(route.getInterfaceName());           // en0
		Network net = route.getNetwork();

		System.out.println(net.toString());                     // 192.168.0.0/24
		System.out.println(net.getAddress().toString());        // 192.168.0.0
		route = router.getRouteForAddress(new IPv4Address("10.0.1.1"));
		System.out.println(route.getMetric());                  // 10
		System.out.println(route.getInterfaceName());           // en1
		net = route.getNetwork();
		System.out.println(net.toString());                     // 10.0.0.0/8
		// for ( Route route : router.getRoutes() ) {
		//    Network net = route.getNetwork();
		//    // other important operations
		// }
    }
}
