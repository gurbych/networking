import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;

public class Router {
    private List<Route> routes;

    public Router(List<Route> routes) {
        this.routes = routes;
    }

    public void addRoute(Route route) {
        this.routes.add(route);
    }

    public Route getRouteForAddress(IPv4Address address) throws InvalidIpException  {
        Route optRoute = null;
        Iterator<Route> it = routes.iterator();

        while ( it.hasNext() && optRoute == null ) {
            Route currentRoute = it.next();

            if ( currentRoute.getNetwork().contains(address) ) {
                optRoute = currentRoute;
            }
        }

        if ( optRoute == null ) {
            return null;
        }

        while ( it.hasNext() ) {
            Route currentRoute = it.next();

            if ( currentRoute.getNetwork().contains(address) ) {
                if ( optRoute.getNetwork().getMaskLength() < currentRoute.getNetwork().getMaskLength() ) {
                    optRoute = currentRoute;
                } else if ( optRoute.getMetric() > currentRoute.getMetric() ) {
                        optRoute = currentRoute;
                }
            }
        }

        return optRoute;
    }

    public List<Route> getRoutes() {
        return this.routes;
    }

    public void removeRoute(Route route) {
        this.routes.remove(route);
    }
}