import java.lang.*;
import java.io.*;
// import java.util.regex.Matcher;
// import java.util.regex.Pattern;

public class IPv4Address {
	private String stringIp;
    private long longIp;

    public IPv4Address(String address) throws InvalidIpException {
        // if ( !IPv4AddressStringValidate(address) ) {
        //     throw new InvalidIpException();
        // }
        this.stringIp = address;
        this.longIp = str2long(address);
    }

    public IPv4Address(long address) throws InvalidIpException {
        if ( address < 0 || address > 0xffffffffL ) {
            throw new InvalidIpException();
        }
        this.longIp = address;
        this.stringIp = long2str(address);
    }

    private static String long2str(long address) {
        return String.format("%s.%s.%s.%s",
            (0xff000000 & address) >> 24,
            (0x00ff0000 & address) >> 16,
            (0x0000ff00 & address) >> 8,
            0x000000ff & address);
    }

    private static long str2long(String address) throws InvalidIpException {
        long ip = 0;
        String[] parts = address.split("\\.");

        try {
            for ( int i = 0, shift = 24; i < 4; i++, shift -= 8 ) {
                long part = Long.parseLong(parts[i]);

                if ( part < 0 || part > 255 ) {
                    throw new InvalidIpException();
                }

                ip += part << shift;
            }
            return ip;
        } catch ( Exception e ) {
            throw new InvalidIpException();
        }
    }


    //--validate IPv4 using RegEx
    // private boolean IPv4AddressStringValidate(String address) throws InvalidIpException {
    //     Pattern IPv4Pattern = Pattern.compile("^(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])$");  
    //     Matcher m = IPv4Pattern.matcher(address);  
    //     return m.matches();
    // }

    // private boolean IPv4AddressStringValidate(String address) throws InvalidIpException {
    //     if (address == null || address.isEmpty()) {
    //         return false;
    //     }
    //     String[] parts = address.split( "\\." );
    //     if ( parts.length != 4 ) {
    //         return false;
    //     }
    //     long[] ip = new long[4];
    //     for ( int i = 0; i < 4; i++ ) {
    //         ip[i] = Integer.parseInt(parts[i]);
    //         if ( (ip[i] < 0) || (ip[i] > 255) ) {
    //             return false;
    //         }
    //     }
    //     if(address.endsWith(".")) {
    //         return false;
    //     }
    //     return true;
    // } 
   
    public String toString() {
        return this.stringIp;
    }

    public long toLong() {
        return this.longIp;
    }

    public boolean lessThan(IPv4Address address) {
        return this.longIp < address.toLong();
    }

    public boolean greaterThan(IPv4Address address) {
        return !lessThan(address);
    }

    public boolean equals(IPv4Address address) {
        return this.longIp == address.toLong();
    }
}