public class Route {
    private Network network;
    private IPv4Address gateway;
    private String interfaceName;
    private int metric;

    public Route(Network network, IPv4Address gateway, String interfaceName, int metric) 
        throws InvalidIpException, ArgumentMissedException {

        if ( network == null || interfaceName == null ) {
            throw new ArgumentMissedException();
        }
        if ( metric < 0 ) {
            throw new IllegalArgumentException("Metric has to be positive");
        }
        this.network = network;
        this.gateway = gateway;
        this.interfaceName = interfaceName;
        this.metric = metric;
    }

    public IPv4Address getGateway() {
        return gateway;
    }

    public String getInterfaceName() {
        return interfaceName;
    }

    public int getMetric() {
        return metric;
    }

    public Network getNetwork() {
        return network;
    }

    public String toString() {
        if ( this.gateway == null ) {
            return String.format("net: %s interface: %s metric: %d", network.toString(), interfaceName, metric);
        }
        return String.format("net: %s gateway: %s interface: %s metric: %d", network.toString(), gateway.toString(), interfaceName, metric);
    }
}