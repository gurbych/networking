public class Network {
    private IPv4Address networkAddress;
    private IPv4Address broadcastAddress;
    private int maskLength;
    private long mask;
    private long invertedMask;
    private static Network[] privateNetworks = null;

    public Network(IPv4Address address, int maskLength) throws InvalidMaskException, InvalidIpException {
        if ( maskLength < 0 || maskLength > 32 ) {
            throw new InvalidMaskException();
        }
        this.maskLength = maskLength;
        this.mask = ~((1L << 32 - this.maskLength) - 1) & 0xffffffffL;
        this.invertedMask = ~this.mask & 0xffffffffL;
        this.networkAddress = new IPv4Address(address.toLong() & this.mask);
        this.broadcastAddress = new IPv4Address(address.toLong() | this.invertedMask);
    }

    public String toString() {
        return String.format("%s/%s", this.networkAddress, this.maskLength);
    }

    public boolean contains(IPv4Address address) throws InvalidIpException {
        return this.networkAddress.toLong() == (address.toLong() & this.mask);
    }

    public IPv4Address getAddress() {
        return this.networkAddress;
    }

    public IPv4Address getBroadcastAddress() {
        return broadcastAddress;
    }

    public IPv4Address getFirstUsableAddress() throws InvalidIpException {
        if (this.maskLength == 32) {
            return this.networkAddress;
        } else if (this.maskLength == 31) {
            return null;
        }
        return new IPv4Address(this.networkAddress.toLong() + 1);
    }

    public IPv4Address getLastUsableAddress() throws InvalidIpException {
        if (this.maskLength == 32) {
            return this.networkAddress;
        } else if (this.maskLength == 31) {
            return null;
        }
        return new IPv4Address(this.broadcastAddress.toLong() - 1); 
    }

    public long getMask() {
        return this.mask;
    }

    public String getMaskString() throws InvalidIpException {
        return (new IPv4Address(this.mask)).toString();
    }

    public int getMaskLength() {
        return this.maskLength;
    }

    public Network[] getSubnets() throws InvalidIpException, InvalidMaskException {
        Network[] subnets = new Network[2];
        int newMaskLength = this.maskLength + 1;

        if ( newMaskLength == 32 ) {
            return subnets;
        }

        subnets[0] = new Network(this.networkAddress, newMaskLength);
        subnets[1] = new Network(this.broadcastAddress, newMaskLength);

        return subnets;
    } // produce two half-sized subnets

    public long getTotalHosts() throws InvalidIpException {
        if ( this.maskLength == 32 ) {
            return 1;
        } else if ( this.maskLength == 31 ) {
            return 0;
        }
        return this.invertedMask - 1;
    } // excluding network and broadcast

    public boolean isPublic() throws InvalidIpException, InvalidMaskException {
        if ( this.privateNetworks == null ) {
            this.privateNetworks = new Network[] {
                new Network(new IPv4Address("10.0.0.0"), 8),
                new Network(new IPv4Address("172.16.0.0"), 12),
                new Network(new IPv4Address("192.168.0.0"), 16),
                new Network(new IPv4Address("127.0.0.0"), 8)
            };
        }

        for ( Network network : privateNetworks ) {
            if ( network.contains(this.networkAddress) ) {
                return false;
            }
        }
        return true; 
    }
}