import java.io.BufferedReader;
import java.io.PrintWriter;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.File;
import java.util.Date;
import java.net.Socket;
import java.net.URI;
import java.net.MalformedURLException;
import java.net.URISyntaxException;

public class Worker implements Runnable {

    private Socket clientSocket;
    private HttpServer serverSocket;

    public Worker(Socket clientSocket, HttpServer serverSocket) {
        this.clientSocket = clientSocket;
        this.serverSocket = serverSocket;
    }

    public void run() {
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(),true);
            String input;

            System.out.println("New socket was opened. Wait for GET request");
            while ((input = in.readLine()) != null) {
                // System.out.println(input);
                if (input.equalsIgnoreCase("disconnect")) {
                    break;
                }
                String[] userRequest = input.split(" ");

                if (userRequest.length == 3 && userRequest[0].equals("GET")) {
                    URI uri = null;
                    try {
                        try {
                            uri = new URI(userRequest[1]);
                        } catch (URISyntaxException e) {
                            throw new BadRequestException();
                        }
                        if (!(input = in.readLine()).equals("\r?\n|\r")) {
                            if (input.startsWith("Host:")) {
                                input = input.substring(6);
                                out.println("Accepted hostname: " + input);
                            } else {
                                throw new BadRequestException();
                            }
                        }
                    } catch (BadRequestException e) {
                        out.println("Client Error: 400 Bad Request");
                        System.out.println("400 Bad Request");
                    }
                    doGet(uri, out);
                } else {
                    out.println("Client Error: 405 Method Not Allowed");
                    System.out.println("405 Method Not Allowed");
                }
            }

            out.close();
            in.close();
            System.out.println("Request processed. Socket was closed");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void doGet(URI uri, PrintWriter out) {
        String extension;
        String path = uri.getPath();
        File file;

        System.out.println(path);

        if ( path.endsWith("/")) {
            path += "index.html";
            extension = "html";
        } else {
            extension = path.substring(path.lastIndexOf(".") + 1);
        }

        file = new File("." + serverSocket.getRootDir() + path);

        if ( file.exists() ) {
            out.println("HTTP/1.1 200 OK");
            out.println("Connection: close");
            out.printf("Content-Length: %d\n", file.length());
            out.printf("Content-Type: %s\n", getContentType(extension));
            out.printf("Date: %1$ta, %1$te, %1$tb, %1$tY %1$tT %1$tZ\n", new Date());

            sendFile(file, out);

            System.out.printf("200 %s", path);
        } else {
            out.println("HTTP/1.1 400 Not Found");
            System.out.printf("404 Not Found %s", path);
        }
    }

    public String getContentType(String ext) {
        try {
            return serverSocket.getMimeTypes().get(ext).toString();
        } catch(Exception e) {
            return "application/octet-stream";
        }
    }

    public void sendFile(File f, PrintWriter out) {
        InputStream file = null;
        int buffer;
        
        try {
            file = new FileInputStream(f.getAbsolutePath());

            while ( (buffer = file.read()) != -1 ) {
                output.write(buffer);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println("File could not be read.");
            e.printStackTrace();
        } finally {
            try {
                if ( file != null ) {
                    file.close();
                }
            } catch (IOException e) {
                System.err.println("File closing failed!");
                e.printStackTrace();
            }
        }
    }
}