import java.io.FileNotFoundException;
import java.net.UnknownHostException;
import java.io.PrintWriter;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class HttpServer implements Runnable {

  private HashMap mimeTypes;
	private int port;
  private InetAddress address;
  private String rootDir;
  private ServerSocket serverSocket;
  private Thread runningThread;
  private ExecutorService threadPool;

	public HttpServer(HashMap conf) throws HttpServerException, UnknownHostException, FileNotFoundException, IOException {
    this.threadPool = Executors.newFixedThreadPool(10);
		this.port = Integer.valueOf((String)conf.get("port"));
    if ( port < 1024 || port > 65535 ) {
      throw new InvalidPortException();
    }
    this.address = InetAddress.getByName((String)conf.get("address"));
    this.rootDir = (String)conf.get("root_dir");
    openServerSocket();
    this.mimeTypes = readConf(new File("mime.types"));
	}

  public int getPort() {
   	return this.port;
  }

  public String getRootDir() {
    return this.rootDir;
  }

  public HashMap getMimeTypes() {
    return this.mimeTypes;
  }

  public void run(){
      synchronized(this){
          this.runningThread = Thread.currentThread();
      }
      while( true ){
          try {
              Socket clientSocket = this.serverSocket.accept();
              this.threadPool.execute(new Worker(clientSocket, this));
          } catch (IOException e) {
              throw new RuntimeException("Error accepting client connection", e);
          }
      }
  }

  private void openServerSocket() {
      try {
          this.serverSocket = new ServerSocket(port, 0, address);
      } catch (IOException e) {
          throw new RuntimeException("Cannot open port", e);
      }
  }

  public static HashMap<String, String> readConf (File file) throws FileNotFoundException, IOException {
    HashMap<String, String> map = new HashMap<String, String>();
    BufferedReader in = new BufferedReader(new FileReader(file));
    String line = "";

    while ((line = in.readLine()) != null) {
      String firstLetter = line.substring(0, 1);

      if ( firstLetter == "#" || firstLetter == " " ) {
        continue;
      }
      String parts[] = line.split("\\s+");
      System.out.println(parts[0]);
      System.out.println(parts[1]);
      map.put(parts[0], parts[1]);
    }
    in.close();
    System.out.println(map.toString());
    return map;
  }

  public static void main(String[] args) throws Exception {
    try {
      HttpServer server = new HttpServer(readConf(new File("http.conf")));
      new Thread(server).start();
      server.run();
    } catch ( IOException e ) {
      e.printStackTrace();
    } catch ( IllegalArgumentException e ) {
      System.err.println(e);
    }
  }
}