# NETWORKING #

1) **IPv4Address**

public class IPv4Address {

   public IPv4Address(String address) throws IllegalArgumentException {} 

   public IPv4Address(long address) throws IllegalArgumentException {} 
   
   public boolean lessThan(IPv4Address address) {}

   public boolean greaterThan(IPv4Address address) {}

   public boolean equals(IPv4Address address) {}
   
   public String toString() {}

   public long toLong() {}

}

2) **Network**

class Network {

  public Network(IPv4Address address, int maskLength) {}

  public boolean contains(IPv4Address address) {}

  public IPv4Address getAddress() {}

  public IPv4Address getBroadcastAddress() {}

  public IPv4Address getFirstUsableAddress() {}

  public IPv4Address getLastUsableAddress() {}

  public long getMask() {}

  public String getMaskString() {}

  public int getMaskLength() {}

  public Network[] getSubnets() {} // produce two half-sized subnets

  public long getTotalHosts() {} // excluding network and broadcast

  public boolean isPublic() {}

}

4) **Router**

class Route {

  public Route(Network network, IPv4Address gateway, String interfaceName, int 
metric) {}
  public IPv4Address getGateway() {}

  public String getInterfaceName() {}

  public int getMetric() {}

  public Network getNetwork() {}

  public String toString() {}

}

class Router {

  public Router(Iterable<Route> routes) {}

  public void addRoute(Route route) {}

  public Route getRouteForAddress(IPv4Address address) {}

  public Iterable<Route> getRoutes() {}

  public void removeRoute(Route route) {}

}

5) **EchoServer**

class EchoServer {

   public EchoServer() {

       this(DEFAULT_PORT);

   }

   public EchoServer(int port);

   public boolean isRunning();

   public int getPort();

   public void start();

   public void stop();

}

6) **Downloader**

принимает аргументом командной строки URL и сохраняет контент по данному урлу в соответствующий файл текущей папки

7) **HttpServer**

Консольный HTTP-сервер. После запуска обрабатывает в бесконечном цикле соединения. Отвечает на GET-запросы, на запросы с остальными методами отвечает с кодом "405 Method Not Allowed". Если файл, соответствующий GET-запросу, не найден - "404 Not Found". Завершается сервер нажатием Ctrl+C. На каждый запрос выводится в консоль цифровой код ответа и URL.

Адрес, порт и корневой каталог HTTP-сервера определяется конфигурацией в файле http.conf (в папке с исполняемым файлом сервера)